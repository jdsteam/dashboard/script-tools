FROM python:2.7

# install text editor
RUN apt-get update
RUN apt-get install nano

# copy source files
WORKDIR /usr/src/app
COPY . .

# install dependency
# RUN pip install --no-cache-dir -r requirements.txt

# prevent container from exiting
CMD ["tail", "-f", "/dev/null"]