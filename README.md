# Script Tools

Repository ini berisi script-script yang digunakan untuk mengolah atau mengambil data
untuk keperluan Dashboard dengan map.

## Docker Container Setup

Build Docker image
```bash
docker build -t asatrya/python:2.7 .
```

Run Docker container

```bash
docker run --name py-script-tools --rm -v ${PWD}:/usr/src/app -d asatrya/python:2.7
```

Get into the container using bash

```bash
docker exec -it py-script-tools bash
```

Docker cleanup (when you have finished your work)

```bash
docker stop py-script-tools
```

## Fetch Data BPS dan Kemendagri

Script untuk mengambil data Wilayah Administratif tingkat Provinsi hingga Kelurahan dari situs https://sig-dev.bps.go.id dalam format CSV.

- Untuk men-download data dan menyimpan dalam folder `data/` dalam 4 buah file: `data/desa.csv`, `data/kecamatan.csv`, `data/kabupaten.csv`, `data/province.csv`

  ```buildoutcfg
  python bps-data/download-csv.py
  ```

- Command ini sama dengan yang sebelumnya yakni untuk mendownload semua data dengan menyatukan hasilnya sesuai dengan kategori seperti provinsi, kabupaten, kota, dan desa.

  ```bash
  python bps-data/download-csv.py autodiscover_combine
  ```

- Untuk mendownload semua data dengan datanya disimpan ke file nama daerahnya dapat dilakukan dengan mengeksekusi

  ```bash
  python bps-data/download-csv.py autodiscover
  ```

## Menampilkan Data ke Layar

Untuk menampilkan data ke layar terminal sesuai dengan pencarian nama daerahnya
- Menampilkan data provinsi di indonesia dengan mengeksekusi
    ```bash
    python bps-data/download-csv.py query
    ```
- Menampilkan data kabupaten/kota di provinsi **XYZ** dengan mengeksekusi
    ```bash
    python bps-data/download-csv.py query XYZ
    ```
- Menampilkan data kecamatan di provinsi **XYZ** dan kabupaten/kota **ABC** dengan mengeksekusi
    ```bash
    python bps-data/download-csv.py query XYZ ABC
    ```
- Menampilkan data desa di provinsi **XYZ**, kabupaten/kota **ABC**, dan kecamatan **JKL** dengan mengeksekusi
    ```bash
    python bps-data/download-csv.py query XYZ ABC JKL
    ```

## Men-Generate perintah SQL create table dan insert untuk membuat relasi dan menambah data

Membuat daftar perintah SQL CREATE TABLE dan SQL INSERT dar situs https://sig-dev.bps.go.id

  Contoh pemanggilan
  - Untuk menampilkan bantuan

    ```bash
    python bps-data/sql_command_creator.py -h
    ```

  - Untuk eksekusi

    - Default
      ```bash
      python bps-data/sql_command_creator.py
      ```

    - Tanpa pembuatan relasi sehingga tidak ada relation creation dan pengaturan serial untuk primary key.
      ```bash
      python bps-data/sql_command_creator.py --withoutrelation True
      ```

    - Tanpa pembuatan data sehingga tidak ada INSERT statement
      ```bash
      python bps-data/sql_command_creator.py --withoutdata True
      ```

    - Mengganti default directory output selain `/tmp/` dengan syarat tabel tersebut harus sudah dibuat.
      ```bash
      python bps-data/sql_command_creator.py -od "<directory>"
      ```

## Meng-Generate perintah SQL update untuk memasukkan data geometri

Membuat daftar perintah SQL UPDATE dari data geojson dan csv. Kompleksitas algoritma yang digunakan adalah **O(m*n)** dengan **m** adalah jumlah data di GEOJSON dan **n** adalah jumlah data di CSV.

  Contoh pemanggilan:
  - Untuk menampilkan bantuan

    ```bash
    python bps-data/geometry-creator.py -h
    ```

  - Eksekusi

    ```bash
    python bps-data/geometry-creator.py [-o <output_file_name>] <geojson_file> <csv_file> <mapping> <table_name>
    ```

    - `<geojson_file>`: posisi file geojson. Contoh file ada di `examples/JABAR_KABKOT_2015_UTM48S.geojson`
    - `<csv_file>`: posisi file csv. Contoh data CSV ada di `examples/kabupaten_jabar.csv`
    - `<level_type>`: Tipe level dari data yang diproses. Nilai parameter yang didukung sekarang: `kabupaten`, `kecamatan`, dan `desa`.
    - `<table_name>`: Nama tabel tujuan.
    - `-o <output_file_name>`: **OPSIONAL** Jika tidak dimasukkan akan dikeluarkan melalui standar output.

    Contoh command:

    ```bash
    python bps-data/geometry-creator.py -o output.sql examples/JABAR_KABKOT_2015_UTM48S.geojson examples/kabupaten_jabar.csv '{"KABKOTNO":"kode_bps"}' table-name
    ```