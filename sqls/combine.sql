SELECT
	prov.id,
	kabko.bps_kode,
	kabko.bps_nama,
	kabko.kemendagri_kode,
	kabko.kemendagri_nama
FROM (SELECT DISTINCT * FROM public.kabko_tmp) AS kabko
INNER JOIN public.provinsi AS prov
	ON trim(prov.bps_kode) = substring(kabko.bps_kode FROM 1 FOR 2)
LIMIT 10;

-- Example insertion
INSERT INTO public.kelurahan (id_kecamatan, bps_kode, bps_nama, kemendagri_kode, kemendagri_nama)
SELECT
	kec.id,
	desa.bps_kode,
	desa.bps_nama,
	desa.kemendagri_kode,
	desa.kemendagri_nama
FROM (SELECT DISTINCT * FROM public.raw_desa) AS desa
INNER JOIN public.kecamatan AS kec
	ON trim(kec.bps_kode) = substring(desa.bps_kode FROM 1 FOR 7);

-- Find duplicate from raw data
SELECT *, COUNT(*) AS count
FROM public.kec_tmp
GROUP BY bps_kode, bps_nama, kemendagri_kode, kemendagri_nama
HAVING COUNT(*) > 1;