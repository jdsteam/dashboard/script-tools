#!/usr/bin/env python

import os, errno
import sys
import argparse
import json, csv, re

def prepare_argument():
    parser = argparse.ArgumentParser(description='Geometry creator for SQL UPDATE statement')
    parser.add_argument("geojson_file", help="GeoJSON of bundle of multipolygon")
    parser.add_argument("csv_file", help="CSV data")
    parser.add_argument("level_type", help="Level type")
    parser.add_argument("table_name", help="Table name destination")
    parser.add_argument('-c', '--checking', help="For checking variable", type=bool, default=False)
    parser.add_argument('-o', '--output', help="Output file")
    return parser

def csv_to_data(filename):
    result = []
    with open(filename, 'r') as csvfile:
        csvread = csv.reader(csvfile, delimiter=',')
        keys = None
        for index, row in enumerate(csvread):
            if index == 0:
                keys = row
            else:
                tmp = {}
                for i, key in enumerate(keys):
                    tmp[re.sub(r'[^\w\ \_\'\"\.]', '', key)] = row[i]
                result.append(tmp)

    return result

def mapping_geojson(level_type, properties):
    if level_type == 'kabupaten':
        return properties['PROVNO'] + properties['KABKOTNO']
    if level_type == 'kecamatan':
        return properties['PROVNO'] + properties['KABKOTNO'] + properties['KECNO']
    if level_type == 'desa':
        return properties['PROVNO'] + properties['KABKOTNO'] + properties['KECNO'] + properties['DESANO']
    return 'geojson_notmatch'

def mapping_csv(level_type, attributes):
    if level_type == 'kabupaten':
        return attributes['kode_bps']
    if level_type == 'kecamatan':
        return attributes['kode_bps']
    if level_type == 'desa':
        return attributes['kode_bps'][:-2]
    return 'csv_notmatch'

def check_all_equal(level_type, left, right):
    return mapping_geojson(level_type, left) == mapping_csv(level_type, right)

def create_sql(geojson, csv, level_type, is_checking, table_name, fdesc):
    for feature in geojson['features']:
        if is_checking:
            print 'Check properties:\n', json.dumps(feature['properties'])
            break

        for csv_item in csv:
            if check_all_equal(level_type, feature['properties'], csv_item):
                if is_checking:
                    break
                else:
                    fdesc.write('UPDATE `' + table_name + '` SET polygon = ST_GeomFromGeoJSON(\'' + \
                        json.dumps(feature['geometry']) + \
                        '\') WHERE bps_kode = \'' + csv_item['kode_bps'] +
                        '\';\n')

if __name__ == "__main__":
    args = prepare_argument().parse_args()
    geojson = None
    with open(args.geojson_file, 'r') as f:
        geojson = json.load(f)
    csv_data = csv_to_data(args.csv_file)
    is_checking = args.checking

    fdesc = sys.stdout
    if args.output is not None:
        fdesc = open(args.output, 'w')

    create_sql(geojson, csv_data, args.level_type, is_checking, args.table_name, fdesc)
