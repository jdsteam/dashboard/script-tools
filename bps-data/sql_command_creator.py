#!/usr/bin/env python

from __future__ import print_function

import sys
from shutil import copyfileobj
import urllib2
import json
import time
import argparse
import re

BPS_CODE_URL = 'https://sig-dev.bps.go.id/restDropDown/getwilayah'
DATA_PERIODE = '20181'
SOURCE_URL = 'https://sig-dev.bps.go.id/restBridging/getwilayahperiode'


def prepare_argument():
    parser = argparse.ArgumentParser(
        description='SQL generator for dashboard')
    parser.add_argument('--withoutrelation',
                        help="Create SQL without relation creation", type=bool, default=False)
    parser.add_argument('--withoutdata',
                        help="Create SQL without insert statement for data", type=bool, default=False)
    parser.add_argument('-od', '--outputdirectory', help="Output directory", default='/tmp/')
    return parser

def create_table(table_name, fk_name=None):
    s_attributes = [
        'id INTEGER NOT NULL PRIMARY KEY',
        'bps_kode VARCHAR(14) NOT NULL',
        'bps_nama VARCHAR(50) NOT NULL',
        'kemendagri_kode VARCHAR(18)',
        'kemendagri_nama VARCHAR(50)',
        'polygon GEOMETRY'
    ]
    if fk_name is not None:
        s_attributes.append(fk_name + ' INTEGER NOT NULL')

    return 'CREATE TABLE IF NOT EXISTS ' + table_name + ' (\n    ' + \
        ',\n    '.join(s_attributes) + \
        '\n);'

def create_foreign_key(table_name, target_ref, attribute_fk, target_attribute_fk):
    return 'ALTER TABLE ' + table_name + ' ' + \
        'ADD CONSTRAINT ' + (table_name + '__' + target_ref + '__fk') + ' ' + \
        'FOREIGN KEY (' + attribute_fk + ') ' + \
        'REFERENCES ' + target_ref + '(' + target_attribute_fk + ') ' + \
        'ON DELETE CASCADE;'

def create_insert(table_name, id, bps_kode, bps_nama, kemendagri_kode, kemendagri_nama, ref=None):
    return 'INSERT INTO ' + table_name + ' VALUES (' + \
        str(id) + ', \'' + bps_kode + '\', \'' + str(bps_nama).strip() + '\', ' + \
        '\'' + kemendagri_kode + '\', \'' + str(kemendagri_nama).strip() + '\'' + \
        ', NULL' + \
        ((', ' + str(ref) ) if ref is not None else '') + ');'

def create_sequence(table_name, start_with):
    sequence_name = table_name + '__id__seq'
    return 'CREATE SEQUENCE ' + sequence_name + ' START WITH ' + str(start_with) + ';\n' + \
        'ALTER TABLE ' + table_name + ' ALTER COLUMN id SET DEFAULT nextval(\'' + sequence_name + '\');'

def normalize_bps_code(val):
    if re.match(r'/^\d+$/', val):
        return val
    return re.sub(r'[^\d].*$', '', val)

def get_next_level(level):
    if level == 'provinsi':
        return 'kabupaten'
    if level == 'kabupaten':
        return 'kecamatan'
    if level == 'kecamatan':
        return 'desa'
    return None

def get_table_name(level):
    if level == 'provinsi':
        return 'provinsi'
    if level == 'kabupaten' or level == 'kokab' or level == 'kabko':
        return 'kokab'
    if level == 'kecamatan':
        return 'kecamatan'
    if level == 'desa' or level == 'kelurahan':
        return 'kelurahan'
    raise Exception('Nama tabel tidak ditemukan')

class SqlCommandCreator():

    def __init__(self, create_table=None, dump_data=None, output_dir=None):
        self._is_create_table = create_table if type(create_table) == bool else True
        self._is_dump_data = dump_data if type(dump_data) == bool else True
        self._output_dir = output_dir if output_dir is not None else '/tmp/'

    def run(self):
        self.counter = {
            'provinsi': 1,
            'kabupaten': 1,
            'kecamatan': 1,
            'desa': 1
        }

        self.prepare_output()

        if self._is_create_table:
            # Create table
            self.create_table()

        if self._is_dump_data:
            self.prepare_data()

        if self._is_create_table:
            # Setup serial
            self.finishing_create_table()

        self.close_temporary_output()
        self.combine_output()

    def create_table(self):
        self.out('creation').write(create_table(get_table_name('provinsi')) + '\n\n')
        self.out('creation').write(create_table(get_table_name('kabupaten'), 'id_provinsi') + '\n\n')
        self.out('creation').write(create_foreign_key(get_table_name('kabupaten'), get_table_name('kabupaten'), 'id_provinsi', 'id') + '\n\n')
        self.out('creation').write(create_table(get_table_name('kecamatan'), 'id_kokab') + '\n\n')
        self.out('creation').write(create_foreign_key(get_table_name('kecamatan'), get_table_name('kabupaten'), 'id_kokab', 'id') + '\n\n')
        self.out('creation').write(create_table(get_table_name('kelurahan'), 'id_kecamatan') + '\n\n')
        self.out('creation').write(create_foreign_key(get_table_name('kelurahan'), get_table_name('kecamatan'), 'id_kecamatan', 'id') + '\n\n')

    def finishing_create_table(self):
        for level in ['provinsi', 'kabupaten', 'kecamatan', 'desa']:
            self.out('finish').write(create_sequence(get_table_name(level), self.counter[level]) + '\n\n')

    def combine_output(self):
        filenames = []

        if self._is_create_table:
            filenames.append(self._output_dir + 'creation.sql')

        if self._is_dump_data:
            filenames.append(self._output_dir + 'province.sql')
            filenames.append(self._output_dir + 'kabko.sql')
            filenames.append(self._output_dir + 'kecamatan.sql')
            filenames.append(self._output_dir + 'kelurahan.sql')

        if self._is_create_table:
            filenames.append(self._output_dir + 'finishing.sql')

        with open(self._output_dir + 'combine.sql', 'w') as outfile:
            for fname in filenames:
                with open(fname, 'r') as infile:
                    for line in infile:
                        outfile.write(line)

    def prepare_data(self):
        self.data_trace()

    def data_trace(self, level=None, parent_id=None, parent_bps_code=None):
        level = level if level is not None else 'provinsi'
        parent_bps_code = parent_bps_code if parent_bps_code is not None else ''

        retval = urllib2.urlopen(SOURCE_URL + '/level/' + level + '/parent/' + parent_bps_code + '/periode/' + DATA_PERIODE)
        data = json.loads(retval.read())

        for item in data:
            if level != 'desa':
                print('Processing ' + level + ': ' + item['nama_bps'])
            self.out(level).write(create_insert(
                get_table_name(level),
                self.counter[level],
                normalize_bps_code(item['kode_bps']),
                item['nama_bps'],
                item['kode_dagri'],
                item['nama_dagri'],
                parent_id
            ) + '\n')

            next_level = get_next_level(level)
            if next_level is not None:
                self.data_trace(next_level, self.counter[level], item['kode_bps'])
            if level == 'provinsi':
                print('Sleep for 10 seconds')
                self.out('provinsi').flush()
                self.out('kabupaten').flush()
                self.out('kecamatan').flush()
                self.out('desa').flush()
                time.sleep(9)
            self.counter[level] += 1

    def prepare_output(self):
        if self._is_create_table:
            self._creation = open(self._output_dir + 'creation.sql', 'w')

        if self._is_dump_data:
            self._province_data = open(self._output_dir + 'province.sql', 'w')
            self._kabko_data = open(self._output_dir + 'kabko.sql', 'w')
            self._kecamatan_data = open(self._output_dir + 'kecamatan.sql', 'w')
            self._kelurahan_data = open(self._output_dir + 'kelurahan.sql', 'w')

        if self._is_create_table:
            self._finishing_data = open(self._output_dir + 'finishing.sql', 'w')

    def close_temporary_output(self):
        if self._is_create_table:
            self._creation.close()

        if self._is_dump_data:
            self._province_data.close()
            self._kabko_data.close()
            self._kecamatan_data.close()
            self._kelurahan_data.close()

        if self._is_create_table:
            self._finishing_data.close()

    def out(self, _type):
        if _type == 'creation':
            return self._creation
        if _type == 'province' or _type == 'provinsi':
            return self._province_data
        if _type == 'kabko' or _type == 'kabupaten' or _type == 'kokab':
            return self._kabko_data
        if _type == 'kecamatan':
            return self._kecamatan_data
        if _type == 'kelurahan' or _type == 'desa':
            return self._kelurahan_data
        if _type == 'finish':
            return self._finishing_data
        raise Exception('Output type is not recognize')

if __name__ == "__main__":
    args = prepare_argument().parse_args()
    obj = SqlCommandCreator(
        create_table=not args.withoutrelation,
        dump_data=not args.withoutdata,
        output_dir=args.outputdirectory
    )
    obj.run()
