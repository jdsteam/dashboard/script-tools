#!/usr/bin/env python

import os
import errno
import sys
import urllib2
import json
import time
import re

BASE_DIR_PATH = 'data/'
BPS_CODE_URL = 'https://sig-dev.bps.go.id/restDropDown/getwilayah'
DATA_PERIODE = '20181'
SOURCE_URL = 'https://sig-dev.bps.go.id/restBridging/getwilayahperiode'


def create_url(provinsi, kabkot, kecamatan):
    # Get Data Provinsi
    if provinsi is None:
        return SOURCE_URL + '/level/provinsi/parent//periode/' + DATA_PERIODE

    # Get Data Kabupaten
    url = BPS_CODE_URL
    retval = urllib2.urlopen(url)
    province_list = json.loads(retval.read())
    kode_provinsi = None
    for item in province_list:
        if item['nama'].lower() == provinsi.lower():
            kode_provinsi = int(item['kode'])
            break
    if kode_provinsi is None:
        raise Exception('Provinsi tidak ditemukan')
    if kabkot is None:
        return SOURCE_URL + '/level/kabupaten/parent/' + str(kode_provinsi) + '/periode/' + DATA_PERIODE

    # Get data kecamatan
    url = BPS_CODE_URL + '/level/kabupaten/parent/' + str(kode_provinsi)
    retval = urllib2.urlopen(url)
    kabkot_list = json.loads(retval.read())
    kode_kabupaten = None
    for item in kabkot_list:
        if item['nama'].lower() == kabkot.lower():
            kode_kabupaten = int(item['kode'])
            break
    if kode_kabupaten is None:
        raise Exception('Kabupaten not found')
    if kecamatan is None:
        return SOURCE_URL + '/level/kecamatan/parent/' + str(kode_kabupaten) + '/periode/' + DATA_PERIODE

    # Get data pedesaan
    url = BPS_CODE_URL + '/level/kecamatan/parent/' + str(kode_kabupaten)
    retval = urllib2.urlopen(url)
    kec_list = json.loads(retval.read())
    kode_kec = None
    for item in kec_list:
        if item['nama'].lower() == kecamatan.lower():
            kode_kec = item['kode']
            break
    if kode_kec is None:
        raise Exception('Kecamatan not found')
    return SOURCE_URL + '/level/desa/parent/' + str(kode_kabupaten) + '/periode/' + DATA_PERIODE


def getdata(url, fdesc, separator=',', with_header=True):
    retval = urllib2.urlopen(url)
    data = json.loads(retval.read())

    if with_header:
        fdesc.write('kode_bps' + separator + 'nama_bps' + separator + 'kode_dagri' + separator + 'nama_dagri\n')
    for item in data:
        fdesc.write(normalize_bps_code(item['kode_bps']) + separator + str(item['nama_bps']).strip() + separator + item['kode_dagri'] + separator + str(item['nama_dagri']).strip() + '\n')

def normalize_bps_code(val):
    if re.match(r'/^\d+$/', val):
        return val
    return re.sub(r'[^\d].*$', '', val)

def auto_discover():
    safe_make_directory()
    with open(create_path('province.csv'), 'w') as f:
        getdata(SOURCE_URL + '/level/provinsi/parent//periode/' + DATA_PERIODE, f)

    url = BPS_CODE_URL
    retval = urllib2.urlopen(url)
    province_list = json.loads(retval.read())

    for province in province_list:
        province_path = province['kode'] + '_' + province['nama'] + '.d/'
        safe_make_directory(province_path)
        with open(create_path(province_path + 'kabkot.csv'), 'w') as f:
            getdata(SOURCE_URL + '/level/kabupaten/parent/' + str(province['kode']) + '/periode/' + DATA_PERIODE, f)

        url = BPS_CODE_URL + '/level/kabupaten/parent/' + str(province['kode'])
        retval = urllib2.urlopen(url)
        kabkot_list = json.loads(retval.read())
        for kabkot in kabkot_list:
            kabkot_path = province_path + kabkot['kode'] + '_' + kabkot['nama'] + '.d/'
            safe_make_directory(kabkot_path)
            with open(create_path(kabkot_path + 'kecamatan.csv'), 'w') as f:
                getdata(SOURCE_URL + '/level/kecamatan/parent/' + str(kabkot['kode']) + '/periode/' + DATA_PERIODE, f)

            url = BPS_CODE_URL + '/level/kecamatan/parent/' + str(kabkot['kode'])
            retval = urllib2.urlopen(url)
            kec_list = json.loads(retval.read())
            for kec in kec_list:
                kec_path = kabkot_path + kec['kode'] + '_' + kec['nama'] + '.d/'
                safe_make_directory(kec_path)
                with open(create_path(kec_path + 'desa.csv'), 'w') as f:
                    getdata(SOURCE_URL + '/level/desa/parent/' + str(kabkot['kode']) + '/periode/' + DATA_PERIODE, f)


def auto_discover_combine():
    separator = ','
    safe_make_directory()
    with open(create_path('province.csv'), 'w') as f:
        getdata(SOURCE_URL + '/level/provinsi/parent//periode/' + DATA_PERIODE, f)

    with open(create_path('kabupaten.csv'), 'w') as kab_f:
        kab_f.write('kode_bps' + separator + 'nama_bps' + separator + 'kode_dagri' + separator + 'nama_dagri\n')
        with open(create_path('kecamatan.csv'), 'w') as kec_f:
            kec_f.write('kode_bps' + separator + 'nama_bps' + separator + 'kode_dagri' + separator + 'nama_dagri\n')
            with open(create_path('desa.csv'), 'w') as desa_f:
                desa_f.write('kode_bps' + separator + 'nama_bps' + separator + 'kode_dagri' + separator + 'nama_dagri\n')

                url = BPS_CODE_URL
                retval = urllib2.urlopen(url)
                province_list = json.loads(retval.read())

                for province in province_list:
                    print 'Processing kabkot in ' + province['nama']
                    getdata(SOURCE_URL + '/level/kabupaten/parent/' + str(province['kode']) + '/periode/' + DATA_PERIODE, kab_f, separator, False)

                    url = BPS_CODE_URL + '/level/kabupaten/parent/' + str(province['kode'])
                    retval = urllib2.urlopen(url)
                    kabkot_list = json.loads(retval.read())

                    for kabkot in kabkot_list:
                        print 'Processing kecamatan and desa in ' + kabkot['nama']
                        getdata(SOURCE_URL + '/level/kecamatan/parent/' + str(kabkot['kode']) + '/periode/' + DATA_PERIODE, kec_f, separator, False)

                        url = BPS_CODE_URL + '/level/kecamatan/parent/' + str(kabkot['kode'])
                        retval = urllib2.urlopen(url)
                        kec_list = json.loads(retval.read())

                        for kec in kec_list:
                            getdata(SOURCE_URL + '/level/desa/parent/' + str(kec['kode']) + '/periode/' + DATA_PERIODE, desa_f, separator, False)

                    print 'Sleep for 10 seconds'
                    kab_f.flush()
                    kec_f.flush()
                    desa_f.flush()
                    time.sleep(9)

def query():
    provinsi = sys.argv[2] if len(sys.argv) > 2 else None
    kabkot = sys.argv[3] if len(sys.argv) > 3 else None
    kecamatan = sys.argv[4] if len(sys.argv) > 4 else None

    data_url = create_url(provinsi, kabkot, kecamatan)
    getdata(data_url, sys.stdout)

def command_not_found():
    print 'Command not found'

def create_path(path):
    if path is None:
        path = ''

    return BASE_DIR_PATH + str(path)


def safe_make_directory(path = None):
    try:
        os.makedirs(create_path(path))
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise


if __name__ == "__main__":
    command = sys.argv[1] if len(sys.argv) > 1 else 'autodiscover_combine'
    commands = {
        'autodiscover': auto_discover,
        'autodiscover_combine': auto_discover_combine,
        'query': query
    }

    commands.get(command.lower(), command_not_found)()
